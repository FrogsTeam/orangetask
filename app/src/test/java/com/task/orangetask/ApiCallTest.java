package com.task.orangetask;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.flikersearch.business.photos.PhotosRepo;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;
import com.task.orangetask.flikersearch.model.repo.GroupsRepoImpl;
import com.task.orangetask.flikersearch.model.repo.PhotosRepoImpl;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class ApiCallTest {


    @Test
    public void testPhotosSearch() {

        PhotosRepoImpl photosRepo = new PhotosRepoImpl();
        try {
            ResponseOfPhotoSearch responseOfPhotoSearch = photosRepo.getPhotos("Cat", 1);
            assertTrue(responseOfPhotoSearch.getPhotos().getPhotos().size() > 0);
            assertTrue(responseOfPhotoSearch.getPhotos().getPhotos().get(0).getTitle().contains("Cat"));
        } catch (OrangeException e) {

        }
    }

    @Test
    public void testGroupsSearch() {

        GroupsRepoImpl groupsRepo = new GroupsRepoImpl();
        try {
            ResponseOfGroupSearch responseOfGroupSearch = groupsRepo.getGroups("Cat", 1);
            assertTrue(responseOfGroupSearch.getGroups().getGroup().size() > 0);
            assertTrue(responseOfGroupSearch.getGroups().getGroup().get(0).getName().contains("Cat"));
        } catch (OrangeException e) {

        }
    }
}
