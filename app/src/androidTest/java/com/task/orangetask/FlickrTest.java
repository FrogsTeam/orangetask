package com.task.orangetask;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.test.RenamingDelegatingContext;
import android.test.ViewAsserts;
import android.view.KeyEvent;
import android.view.View;

import com.task.orangetask.flikersearch.ui.activity.SearchActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class FlickrTest {

    @Rule
    public ActivityTestRule<SearchActivity> mActivityRule =
            new ActivityTestRule<SearchActivity>(SearchActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    Context targetContext = InstrumentationRegistry.getInstrumentation()
                            .getTargetContext();
                    Intent result = new Intent(targetContext, MainActivity.class);
                    result.putExtra("search_type", 1);
                    return result;
                }
            };


    @Test
    public void passExtraInIntent() {

        assertNotNull(mActivityRule.getActivity().getIntent().getExtras());


    }


    @Test
    public void testSearch() {
        onView(withId(R.id.action_search)).perform(click());
        Instrumentation instrumentation = getInstrumentation();
        instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_SEARCH);
        instrumentation.sendCharacterSync(KeyEvent.KEYCODE_C);
        instrumentation.sendCharacterSync(KeyEvent.KEYCODE_A);
        instrumentation.sendCharacterSync(KeyEvent.KEYCODE_T);
        instrumentation.sendCharacterSync(KeyEvent.KEYCODE_ENTER);

        View o = mActivityRule.getActivity().getWindow().getDecorView();

        ViewAsserts.assertOnScreen(o, mActivityRule.getActivity().findViewById(R.id.progressBar));

    }


}
