package com.task.orangetask.util;

import android.util.Log;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.base.exception.UiException;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class MyLogger {


    private static final String TAG = MyLogger.class.getName();

    public static void log(Class c, String functionName, int parameterInt, String parameterString) {
        Log.i(TAG, functionName + " is called form class " + c.getClass().getName());

        Log.d(TAG, parameterInt + "");
        Log.d(TAG, parameterString + "");


    }

    public static void log(Class c, String functionName, String parameterString) {
        Log.i(TAG, functionName + " is called form class " + c.getClass().getName());

        Log.d(TAG, parameterString + "");


    }

    public static void log(Class c, String functionName) {
        Log.i(TAG, functionName + " is called form class " + c.getClass().getName());

    }


    public static void logCatch(Class c, String functionName, OrangeException e) {

        Log.e(TAG, "class name : " + c.getName() + " function name : " + functionName + e.getMessage());
    }

    public static void logCatch(Class c, String functionName, UiException e) {

        Log.e(TAG, "class name : " + c.getName() + " function name : " + functionName + e.getMessage());
    }

}
