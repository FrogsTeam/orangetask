package com.task.orangetask.util;

import android.content.Context;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.task.orangetask.R;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class CashImage {


    public static void cashImage(Context context, String imageUrl, int placeHolder, ImageView imageView) {


        ImageLoader imageLoader = ImageLoader.getInstance();
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisc(true).resetViewBeforeLoading(true)
//                .showImageForEmptyUri(R.mipmap.ic_launcher)
//                .showImageOnFail(R.mipmap.ic_launcher)
//                .showImageOnLoading(R.mipmap.ic_launcher).build();

        DisplayImageOptions  defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true).cacheOnDisk(true).showImageOnLoading(R.drawable.place_holder)
                .resetViewBeforeLoading(true).considerExifParams(true).considerExifParams(true)
                .build();


        if (imageUrl == null || imageUrl.isEmpty() || imageUrl == "")
            return;
//        Glide.with(context)
//                .load(imageUrl).placeholder(placeHolder).error(placeHolder)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imageView);

//        Picasso.with(context)
//                .load(imageUrl)
//                .into(imageView);

        imageLoader.displayImage(imageUrl, imageView, defaultOptions);


    }
}
