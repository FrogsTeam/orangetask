package com.task.orangetask.flikersearch.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.task.orangetask.R;
import com.task.orangetask.flikersearch.model.dto.Photos.Photo;
import com.task.orangetask.util.CashImage;
import com.task.orangetask.util.MyLogger;

import java.util.ArrayList;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    private static final String TAG = PhotosAdapter.class.getName();
    ArrayList<Photo> mData;
    private Context context;
    int lastPosition = -1;

    public PhotosAdapter(ArrayList<Photo> mData) {
        this.mData = mData;
    }

    @Override

    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_item_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        context = parent.getContext();
        return vh;

    }

    @Override
    public void onBindViewHolder(PhotosAdapter.ViewHolder holder, int position) {
        MyLogger.log(PhotosAdapter.class, "onBindViewHolder", position,"");

        CashImage.cashImage(context, createImageURL(mData.get(position)), R.mipmap.ic_launcher, holder.image);
        setAnimation(holder.container, position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        LinearLayout container;

        public ViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            container = (LinearLayout) view.findViewById(R.id.container);

        }
    }

    private String createImageURL(Photo photo) {

        MyLogger.log(PhotosAdapter.class, "createImageURL", photo.toString());

        String imageUrl = "https://farm" + photo.getFarm() + ".staticflickr.com/" + photo.getServer() + "/" + photo.getId() + "_" + photo.getSecret() + "_n.jpg";


        return imageUrl;
    }

    public void addPhotos(ArrayList<Photo> mData) {
        MyLogger.log(PhotosAdapter.class, "addPhotos");

        this.mData.addAll(mData);
        notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate, int position) {
        MyLogger.log(PhotosAdapter.class, "addPhotos");

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
