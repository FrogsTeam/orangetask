package com.task.orangetask.flikersearch.model.dto.groups;

import java.util.ArrayList;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class Groups {


    private int page;
    private int pages;
    private int perpage;
    private int total;
    private ArrayList<Group> group;


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<Group> getGroup() {
        return group;
    }

    public void setGroup(ArrayList<Group> group) {
        this.group = group;
    }
}
