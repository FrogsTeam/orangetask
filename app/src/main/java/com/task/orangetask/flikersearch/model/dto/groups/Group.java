package com.task.orangetask.flikersearch.model.dto.groups;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class Group {


    private String nsid;
    private String name;
    private int eighteenplus;
    private int iconserver;
    private int iconfarm;
    private int members;
    private int pool_count;
    private int topic_count;
    private int privacy;


    public String getNsid() {
        return nsid;
    }

    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEighteenplus() {
        return eighteenplus;
    }

    public void setEighteenplus(int eighteenplus) {
        this.eighteenplus = eighteenplus;
    }

    public int getIconserver() {
        return iconserver;
    }

    public void setIconserver(int iconserver) {
        this.iconserver = iconserver;
    }

    public int getIconfarm() {
        return iconfarm;
    }

    public void setIconfarm(int iconfarm) {
        this.iconfarm = iconfarm;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public int getPool_count() {
        return pool_count;
    }

    public void setPool_count(int pool_count) {
        this.pool_count = pool_count;
    }

    public int getTopic_count() {
        return topic_count;
    }

    public void setTopic_count(int topic_count) {
        this.topic_count = topic_count;
    }

    public int getPrivacy() {
        return privacy;
    }

    public void setPrivacy(int privacy) {
        this.privacy = privacy;
    }

    @Override
    public String toString() {
        return "nsid : " + nsid +
                "  name : " + name +
                "  eighteenplus :  " + eighteenplus +
                "  iconserver :  " + iconserver +
                "  iconfarm  :  " + iconfarm +
                "  members :  " + members +
                " pool_count :  " + pool_count +
                " topic_count :  " + topic_count +
                " privacy :  " + privacy;

    }
}
