package com.task.orangetask.flikersearch.business.photos;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface PhotosRepo {

    ResponseOfPhotoSearch getPhotos(String searchKeyword, int pageNumber ) throws OrangeException;
}
