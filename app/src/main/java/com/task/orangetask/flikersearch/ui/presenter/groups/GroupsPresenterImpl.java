package com.task.orangetask.flikersearch.ui.presenter.groups;

import android.util.Log;

import com.task.orangetask.flikersearch.business.groups.GroupsBusiness;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;
import com.task.orangetask.util.MyLogger;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class GroupsPresenterImpl extends GroupsPresenter {

    private static final String TAG = GroupsPresenterImpl.class.getName();
    private GroupsBusiness business = new GroupsBusiness();


    @Override
    public void getGroups(final String searchKeyWord, final int pageNumber) {
        MyLogger.log(GroupsPresenterImpl.class,"getGroups",pageNumber,searchKeyWord);

        getView().showInlineLoading();
        performActionAsync(new Action<ResponseOfGroupSearch>() {
            @Override
            public ResponseOfGroupSearch run() throws Throwable {
                return business.getGroups(searchKeyWord, pageNumber);
            }
        }, new Callback<ResponseOfGroupSearch>() {
            @Override
            public void onSuccess(ResponseOfGroupSearch result) {
                if (!isViewAttached())
                    return;

                getView().hideInlineLoading();


                if (pageNumber==1){

                    if (!result.getStat().equals("ok"))
                    {
                        getView().serverProblemFirstTime();
                        return;

                    }

                    if (result.getGroups().getGroup().size() == 0) {

                        getView().noDataFoundFirstTime();
                        return;
                    }
                    getView().displayGroupsFirstTime(result.getGroups());

                }else {


                    if (!result.getStat().equals("ok"))
                    {
                        getView().serverProblem();
                        return;

                    }

                    if (result.getGroups().getGroup().size() == 0) {

                        getView().noDataFound();
                        return;
                    }
                    getView().displayGroups(result.getGroups());

                }


            }

            @Override
            public void onError(Throwable error) {
                getView().hideInlineLoading();

                if (pageNumber == 1) {
                    getView().showInlineErrorFirstTime();
                } else {
                    getView().showInlineError();
                }
                Log.d(TAG, "onError() returned: " + error.getMessage());


            }
        });

    }
}


