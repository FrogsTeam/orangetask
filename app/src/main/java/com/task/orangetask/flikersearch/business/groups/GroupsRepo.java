package com.task.orangetask.flikersearch.business.groups;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface GroupsRepo {

    ResponseOfGroupSearch getGroups(String searchKeyword, int pageNumber) throws OrangeException;
}
