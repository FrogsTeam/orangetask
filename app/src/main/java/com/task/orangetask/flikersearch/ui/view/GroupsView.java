package com.task.orangetask.flikersearch.ui.view;

import com.task.orangetask.base.view.MvpView;
import com.task.orangetask.flikersearch.model.dto.groups.Groups;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface GroupsView extends MvpView {


    void showInlineLoading();

    void hideInlineLoading();

    void showInlineError();

    void displayGroups(Groups groups);

    void showInlineErrorFirstTime();

    void displayGroupsFirstTime(Groups groups);

    void noDataFound();
    void noDataFoundFirstTime();

    void serverProblem();

    void serverProblemFirstTime();


}
