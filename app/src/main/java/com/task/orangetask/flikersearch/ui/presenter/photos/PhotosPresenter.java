package com.task.orangetask.flikersearch.ui.presenter.photos;

import com.task.orangetask.flikersearch.ui.view.PhotosView;
import com.task.orangetask.base.presenter.BasePresenter;

/**
 * Created by eslamhusseinawad on 5/14/16.
 */
public abstract class PhotosPresenter extends BasePresenter<PhotosView> {

    public abstract void getPhotos(String searchKeyWord,int pageNumber);

}
