package com.task.orangetask.flikersearch.model.dto.groups;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class ResponseOfGroupSearch {

    private Groups groups;
    private String stat;

    public Groups getGroups() {
        return groups;
    }

    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
