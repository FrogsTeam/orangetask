package com.task.orangetask.flikersearch.model.cloud.groups;

import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface GroupsSearchService {


    @GET("?method=flickr.groups.search&format=json&nojsoncallback=1")
    Call<ResponseOfGroupSearch> getGroups(@Query("text") String keywordSearch, @Query("page") int pageNumber, @Query("api_key") String api_key);
}
