package com.task.orangetask.flikersearch.ui.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task.orangetask.R;
import com.task.orangetask.base.view.BaseFragment;
import com.task.orangetask.flikersearch.model.dto.Photos.Photos;
import com.task.orangetask.flikersearch.ui.adapter.PhotosAdapter;
import com.task.orangetask.flikersearch.ui.presenter.photos.PhotosPresenter;
import com.task.orangetask.flikersearch.ui.presenter.photos.PhotosPresenterImpl;
import com.task.orangetask.flikersearch.ui.view.PhotosView;
import com.task.orangetask.util.EndlessScrollRecyclListener;
import com.task.orangetask.util.KeyboardUtils;


public class PhotosSearchActivityFragment extends BaseFragment<PhotosPresenter> implements PhotosView {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private static final String ARG_SEARCH_KEY_WORD = "search_keyword";
    private static final String TAG = "photosearchfragment";

    private String searchKeyword;
    Photos mData = new Photos();

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView errorTxt;

    private RelativeLayout error_layout;
    private ImageView error_image_view;
    PhotosAdapter adapter;
    GridLayoutManager layoutManager;

    //scroll to load more

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    // scroll to load more

    public PhotosSearchActivityFragment() {
    }

    public static PhotosSearchActivityFragment newInstance(String searchKeyword) {
        PhotosSearchActivityFragment fragment = new PhotosSearchActivityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SEARCH_KEY_WORD, searchKeyword);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected PhotosPresenter createPresenter() {
        return new PhotosPresenterImpl();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchKeyword = getArguments().getString(ARG_SEARCH_KEY_WORD);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_photos_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        error_layout = (RelativeLayout) view.findViewById(R.id.error_layout);
        error_image_view = (ImageView) view.findViewById(R.id.error_image_view);
        errorTxt = (TextView) view.findViewById(R.id.error_text_view);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setTitle(getActivity().getResources().getString(R.string.search_in_photos));


        recyclerView = (RecyclerView) view.findViewById(R.id.photos_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getActivity(), 4);

        recyclerView.setLayoutManager(layoutManager);

        setUpErrorView(getActivity().getResources().getString(
                R.string.search_in_photos), R.drawable.ic_search_black_48dp);

        recyclerView.setOnScrollListener(new EndlessScrollRecyclListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {


                Log.d(TAG, "onLoadMore() returned getPages : " + mData.getPages());

                Log.d(TAG, "onLoadMore() returned page : " + page);
                Log.d(TAG, "onLoadMore() returned totalItemsCount : " + totalItemsCount);

                //// TODO: 5/17/16 handel when stop
                getPhotos(searchKeyword, page);


            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_photos_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    searchKeyword = query;
                    getPhotos(searchKeyword, 1);
                    KeyboardUtils.hideSoftKeyboard(getActivity());
                    error_layout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }

    private void getPhotos(String iSearchKeyword, int pageNumber) {
        getPresenter().getPhotos(iSearchKeyword, pageNumber);
    }

    @Override
    public void showInlineLoading() {

        if (progressBar == null)
            progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);


    }

    @Override
    public void hideInlineLoading() {
        if (progressBar == null)
            progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);


    }

    @Override
    public void showInlineError() {

        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.no_internet_connection_please_check_your_internet_connection),
                Snackbar.LENGTH_LONG).show();


    }

    @Override
    public void displayPhotos(Photos photos) {
        mData.setPage(photos.getPage());
        mData.setPages(photos.getPages());
        mData.setPerPage(photos.getPerPage());
        mData.getPhotos().addAll(photos.getPhotos());
        mData.setTotal(photos.getTotal());
        adapter.addPhotos(photos.getPhotos());
    }

    @Override
    public void showInlineErrorFirstTime() {


        recyclerView.setVisibility(View.INVISIBLE);
        setUpErrorView(getActivity().getResources().getString(
                R.string.no_internet_connection_please_check_your_internet_connection), R.drawable.no_internet_connection);
        error_layout.setVisibility(View.VISIBLE);


    }

    @Override
    public void displayPhotosFirstTime(Photos photos) {
        error_layout.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);

        mData = photos;
        adapter = new PhotosAdapter(mData.getPhotos());
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void noDataFound() {
        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.no_data_found),
                Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void noDataFoundFirstTime() {
        setUpErrorView(getActivity().getResources().getString(R.string.no_data_found), R.drawable.ic_search_black_48dp);
        recyclerView.setVisibility(View.INVISIBLE);
        error_layout.setVisibility(View.VISIBLE);


    }

    @Override
    public void serverProblem() {
        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.server_error_connection),
                Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void serverProblemFirstTime() {
        setUpErrorView(getActivity().getResources().getString(R.string.server_error_connection), R.drawable.no_internet_connection);
        recyclerView.setVisibility(View.INVISIBLE);
        error_layout.setVisibility(View.VISIBLE);


    }


    private void setUpErrorView(String text, int image) {
        error_image_view.setImageResource(image);
        errorTxt.setText(text);
    }
}
