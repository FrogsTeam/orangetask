package com.task.orangetask.flikersearch.ui.presenter.groups;

import com.task.orangetask.base.presenter.BasePresenter;
import com.task.orangetask.flikersearch.ui.view.GroupsView;
import com.task.orangetask.flikersearch.ui.view.PhotosView;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public abstract class GroupsPresenter extends BasePresenter<GroupsView> {

    public abstract void getGroups(String searchKeyWord,int pageNumber);

}
