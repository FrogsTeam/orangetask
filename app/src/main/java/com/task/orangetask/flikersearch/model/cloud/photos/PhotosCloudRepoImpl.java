package com.task.orangetask.flikersearch.model.cloud.photos;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.base.repo.cloud.BaseCloudRepo;
import com.task.orangetask.base.repo.cloud.CloudConfig;
import com.task.orangetask.flikersearch.business.photos.PhotosRepo;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.util.MyLogger;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class PhotosCloudRepoImpl extends BaseCloudRepo implements PhotosRepo {
    @Override
    public ResponseOfPhotoSearch getPhotos(String searchKeyword, int pageNumber) throws OrangeException {

        MyLogger.log(PhotosCloudRepoImpl.class, "getPhotos", pageNumber, searchKeyword);

        try {
            return execute(create(PhotosSearchService.class).getPhotos(searchKeyword, pageNumber, CloudConfig.FLIKER_KEY));
        } catch (Throwable e) {
            MyLogger.logCatch(PhotosCloudRepoImpl.class, "getPhotos", OrangeException.map(e));

            throw OrangeException.map(e);
        }
    }
}
