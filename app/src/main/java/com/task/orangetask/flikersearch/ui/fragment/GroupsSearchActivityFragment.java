package com.task.orangetask.flikersearch.ui.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task.orangetask.R;
import com.task.orangetask.base.view.BaseFragment;
import com.task.orangetask.flikersearch.model.dto.groups.Groups;
import com.task.orangetask.flikersearch.ui.adapter.GroupsAdapter;
import com.task.orangetask.flikersearch.ui.presenter.groups.GroupsPresenter;
import com.task.orangetask.flikersearch.ui.presenter.groups.GroupsPresenterImpl;
import com.task.orangetask.flikersearch.ui.view.GroupsView;
import com.task.orangetask.util.EndlessScrollRecyclListener;
import com.task.orangetask.util.KeyboardUtils;


public class GroupsSearchActivityFragment extends BaseFragment<GroupsPresenter> implements GroupsView {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private static final String ARG_SEARCH_KEY_WORD = "search_keyword";
    private static final String TAG = GroupsSearchActivityFragment.class.getName();

    private String searchKeyword;
    Groups mData = new Groups();

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView errorTxt;

    private RelativeLayout error_layout;
    private ImageView error_image_view;
    GroupsAdapter adapter;
    GridLayoutManager
            layoutManager;


    public GroupsSearchActivityFragment() {
    }

    public static GroupsSearchActivityFragment newInstance(String searchKeyword) {
        GroupsSearchActivityFragment fragment = new GroupsSearchActivityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SEARCH_KEY_WORD, searchKeyword);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected GroupsPresenterImpl createPresenter() {
        return new GroupsPresenterImpl();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchKeyword = getArguments().getString(ARG_SEARCH_KEY_WORD);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_groups_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        error_layout = (RelativeLayout) view.findViewById(R.id.error_layout);
        error_image_view = (ImageView) view.findViewById(R.id.error_image_view);
        errorTxt = (TextView) view.findViewById(R.id.error_text_view);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setTitle(getActivity().getResources().getString(R.string.search_in_groups));


        recyclerView = (RecyclerView) view.findViewById(R.id.groups_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(activity, 3);

        recyclerView.setLayoutManager(layoutManager);

        setUpErrorView(getActivity().getResources().getString(
                R.string.search_in_groups), R.drawable.ic_search_black_48dp);

        recyclerView.setOnScrollListener(new EndlessScrollRecyclListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {


                Log.d(TAG, "onLoadMore() returned getPages : " + mData.getPages());

                Log.d(TAG, "onLoadMore() returned page : " + page);
                Log.d(TAG, "onLoadMore() returned totalItemsCount : " + totalItemsCount);

                //// TODO: 5/17/16 handel when stop
                getGroups(searchKeyword, page);


            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_groups_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    searchKeyword = query;
                    getGroups(searchKeyword, 1);
                    KeyboardUtils.hideSoftKeyboard(getActivity());
                    error_layout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }

    private void getGroups(String iSearchKeyword, int pageNumber) {
        getPresenter().getGroups(iSearchKeyword, pageNumber);
    }

    @Override
    public void showInlineLoading() {

        if (progressBar == null)
            progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);


    }

    @Override
    public void hideInlineLoading() {
        if (progressBar == null)
            progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showInlineError() {

        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.no_internet_connection_please_check_your_internet_connection),
                Snackbar.LENGTH_LONG).show();


    }

    @Override
    public void displayGroups(Groups groups) {
        mData.setPage(groups.getPage());
        mData.setPages(groups.getPages());
        mData.setPerpage(groups.getPerpage());
        mData.getGroup().addAll(groups.getGroup());
        mData.setTotal(groups.getTotal());
        adapter.addGroups(groups.getGroup());
    }

    @Override
    public void showInlineErrorFirstTime() {


        recyclerView.setVisibility(View.INVISIBLE);
        setUpErrorView(getActivity().getResources().getString(
                R.string.no_internet_connection_please_check_your_internet_connection), R.drawable.no_internet_connection);
        error_layout.setVisibility(View.VISIBLE);


    }

    @Override
    public void displayGroupsFirstTime(Groups groups) {
        error_layout.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);

        mData = groups;
        adapter = new GroupsAdapter(mData.getGroup());
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void noDataFound() {
        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.no_data_found),
                Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void noDataFoundFirstTime() {
        setUpErrorView(getActivity().getResources().getString(R.string.no_data_found), R.drawable.ic_search_black_48dp);
        recyclerView.setVisibility(View.INVISIBLE);
        error_layout.setVisibility(View.VISIBLE);


    }

    @Override
    public void serverProblem() {
        Snackbar.make(getView(), getActivity().getResources().getString(
                R.string.server_error_connection),
                Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void serverProblemFirstTime() {
        setUpErrorView(getActivity().getResources().getString(R.string.server_error_connection), R.drawable.no_internet_connection);
        recyclerView.setVisibility(View.INVISIBLE);
        error_layout.setVisibility(View.VISIBLE);


    }


    private void setUpErrorView(String text, int image) {
        error_image_view.setImageResource(image);
        errorTxt.setText(text);
    }
}
