package com.task.orangetask.flikersearch.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import com.task.orangetask.MainActivity;
import com.task.orangetask.R;
import com.task.orangetask.flikersearch.ui.fragment.GroupsSearchActivityFragment;
import com.task.orangetask.flikersearch.ui.fragment.PhotosSearchActivityFragment;
import com.task.orangetask.util.FragmentUtils;
import com.task.orangetask.util.MyLogger;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyLogger.log(SearchActivity.class, "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_search);


        int searchType = getIntent().getExtras().getInt(MainActivity.SEARCH_TYPE_KEY);


        switch (searchType) {
            case 1:
                FragmentUtils.replaceFragment(SearchActivity.this, PhotosSearchActivityFragment.newInstance(""), R.id.fragment_content, false, "");
                break;
            case 2:
                FragmentUtils.replaceFragment(SearchActivity.this, GroupsSearchActivityFragment.newInstance(""), R.id.fragment_content, false, "");
                break;
            default:
                break;
        }

    }


}
