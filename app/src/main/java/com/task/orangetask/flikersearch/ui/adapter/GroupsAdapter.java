package com.task.orangetask.flikersearch.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.task.orangetask.R;


import com.task.orangetask.flikersearch.model.dto.Photos.Photo;
import com.task.orangetask.flikersearch.model.dto.groups.Group;
import com.task.orangetask.util.CashImage;
import com.task.orangetask.util.MyLogger;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

    private static final String TAG = GroupsAdapter.class.getName();
    ArrayList<Group> mData;
    private Context context;
    private int lastPosition = -1;

    public GroupsAdapter(ArrayList<Group> mData) {
        this.mData = mData;
    }

    @Override
    public GroupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_item_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        context = parent.getContext();
        return vh;

    }

    @Override
    public void onBindViewHolder(GroupsAdapter.ViewHolder holder, int position) {
        MyLogger.log(GroupsAdapter.class, "onBindViewHolder", position,"");

        holder.groupName.setText(mData.get(position).getName());

        CashImage.cashImage(context, createImageURL(mData.get(position)), R.mipmap.ic_launcher, holder.groupImageView);
        setAnimation(holder.container, position);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView groupImageView;
        TextView groupName;
        CardView container;

        public ViewHolder(View view) {
            super(view);
            groupName = (TextView) view.findViewById(R.id.group_name_text_view);
            groupImageView = (CircleImageView) view.findViewById(R.id.group_icon_image_view);
            container = (CardView) view.findViewById(R.id.container);

        }
    }


    public void addGroups(ArrayList<Group> mData) {
        MyLogger.log(GroupsAdapter.class, "addGroups");

        this.mData.addAll(mData);
        notifyDataSetChanged();
    }


    private String createImageURL(Group group) {
        MyLogger.log(GroupsAdapter.class, "createImageURL", group.toString());


        String imageUrl = "https://farm" + group.getIconfarm() + ".staticflickr.com/" + group.getIconserver() + "/buddyicons/" + group.getNsid() + ".jpg";
        return imageUrl;
    }

    private void setAnimation(View viewToAnimate, int position) {
        MyLogger.log(GroupsAdapter.class, "setAnimation", position, "");

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
