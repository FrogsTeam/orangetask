package com.task.orangetask.flikersearch.model.repo;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.flikersearch.business.photos.PhotosRepo;
import com.task.orangetask.flikersearch.model.cloud.photos.PhotosCloudRepoImpl;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.util.MyLogger;


/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class PhotosRepoImpl implements PhotosRepo {

    @Override
    public ResponseOfPhotoSearch getPhotos(String searchKeyword, int pageNumber) throws OrangeException {
        MyLogger.log(PhotosRepoImpl.class,"getPhotos",pageNumber,searchKeyword);

        PhotosRepo cloudRepo = new PhotosCloudRepoImpl();
        return cloudRepo.getPhotos(searchKeyword, pageNumber);

    }
}
