package com.task.orangetask.flikersearch.model.repo;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.flikersearch.business.groups.GroupsRepo;
import com.task.orangetask.flikersearch.model.cloud.groups.GroupsCloudRepoImpl;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;
import com.task.orangetask.util.MyLogger;

/**
 * Created by Eslam Hussein on 5/17/16.
 */
public class GroupsRepoImpl  implements GroupsRepo{

    @Override
    public ResponseOfGroupSearch getGroups(String searchKeyword, int pageNumber) throws OrangeException {
        MyLogger.log(GroupsRepoImpl.class,"getGroups",pageNumber,searchKeyword);

        GroupsRepo cloudRepo = new GroupsCloudRepoImpl();
        return cloudRepo.getGroups(searchKeyword, pageNumber);

    }
}
