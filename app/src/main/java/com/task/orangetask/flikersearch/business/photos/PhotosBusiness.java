package com.task.orangetask.flikersearch.business.photos;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.base.exception.UiException;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.flikersearch.model.repo.PhotosRepoImpl;
import com.task.orangetask.util.MyLogger;


/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class PhotosBusiness {

    private PhotosRepo repo = new PhotosRepoImpl();


    public ResponseOfPhotoSearch getPhotos(String searchKeyword, int pageNumber) throws UiException {

        MyLogger.log(PhotosBusiness.class,"getPhotos",pageNumber,searchKeyword);

        try {
            return repo.getPhotos(searchKeyword, pageNumber);
        } catch (OrangeException e) {
            MyLogger.logCatch(PhotosBusiness.class,"getPhotos",e);

            throw UiException.map(e);
        }
    }

}
