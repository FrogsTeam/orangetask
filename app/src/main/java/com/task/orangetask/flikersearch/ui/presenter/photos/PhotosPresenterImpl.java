package com.task.orangetask.flikersearch.ui.presenter.photos;

import android.util.Log;

import com.task.orangetask.flikersearch.business.photos.PhotosBusiness;
import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;
import com.task.orangetask.util.MyLogger;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class PhotosPresenterImpl extends PhotosPresenter {

    private static final String TAG = PhotosPresenterImpl.class.getName();
    private PhotosBusiness business = new PhotosBusiness();


    @Override
    public void getPhotos(final String searchKeyWord, final int pageNumber) {

        MyLogger.log(PhotosPresenterImpl.class,"getPhotos",pageNumber,searchKeyWord);

        getView().showInlineLoading();
        performActionAsync(new Action<ResponseOfPhotoSearch>() {
            @Override
            public ResponseOfPhotoSearch run() throws Throwable {
                return business.getPhotos(searchKeyWord, pageNumber);
            }
        }, new Callback<ResponseOfPhotoSearch>() {
            @Override
            public void onSuccess(ResponseOfPhotoSearch result) {
                if (!isViewAttached())
                    return;

                getView().hideInlineLoading();


                if (pageNumber==1){

                    if (!result.getStat().equals("ok"))
                    {
                        getView().serverProblemFirstTime();
                        return;

                    }

                    if (result.getPhotos().getPhotos().size() == 0) {

                        getView().noDataFoundFirstTime();
                        return;
                    }
                    getView().displayPhotosFirstTime(result.getPhotos());

                }else {


                    if (!result.getStat().equals("ok"))
                    {
                        getView().serverProblem();
                        return;

                    }

                    if (result.getPhotos().getPhotos().size() == 0) {

                        getView().noDataFound();
                        return;
                    }
                    getView().displayPhotos(result.getPhotos());

                }


            }

            @Override
            public void onError(Throwable error) {
                getView().hideInlineLoading();

                if (pageNumber == 1) {
                    getView().showInlineErrorFirstTime();
                } else {
                    getView().showInlineError();
                }
                Log.d(TAG, "onError() returned: " + error.getMessage());


            }
        });

    }
}


