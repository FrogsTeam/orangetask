package com.task.orangetask.flikersearch.model.dto.Photos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class Photos {

    private int page;
    private int pages;
    @SerializedName("perpage")
    private int perPage;
    private String total;
    @SerializedName("photo")
    private ArrayList<Photo> photos;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}
