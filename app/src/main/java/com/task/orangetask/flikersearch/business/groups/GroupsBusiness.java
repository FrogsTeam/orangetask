package com.task.orangetask.flikersearch.business.groups;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.base.exception.UiException;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;
import com.task.orangetask.flikersearch.model.repo.GroupsRepoImpl;
import com.task.orangetask.util.MyLogger;


/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class GroupsBusiness {

    private GroupsRepo repo = new GroupsRepoImpl();


    public ResponseOfGroupSearch getGroups(String searchKeyword, int pageNumber) throws UiException {
        MyLogger.log(GroupsBusiness.class, "getGroups", pageNumber, searchKeyword);
        try {
            return repo.getGroups(searchKeyword, pageNumber);
        } catch (OrangeException e) {
            MyLogger.logCatch(GroupsBusiness.class,"getGroups",e);
            throw UiException.map(e);
        }
    }

}
