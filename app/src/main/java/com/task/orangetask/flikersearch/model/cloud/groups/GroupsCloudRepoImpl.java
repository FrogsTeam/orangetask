package com.task.orangetask.flikersearch.model.cloud.groups;

import com.task.orangetask.base.exception.OrangeException;
import com.task.orangetask.base.repo.cloud.BaseCloudRepo;
import com.task.orangetask.base.repo.cloud.CloudConfig;
import com.task.orangetask.flikersearch.business.groups.GroupsRepo;
import com.task.orangetask.flikersearch.model.dto.groups.ResponseOfGroupSearch;
import com.task.orangetask.util.MyLogger;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class GroupsCloudRepoImpl extends BaseCloudRepo implements GroupsRepo {
    @Override
    public ResponseOfGroupSearch getGroups(String searchKeyword, int pageNumber) throws OrangeException {
        try {
            MyLogger.log(GroupsCloudRepoImpl.class,"getGroups",pageNumber,searchKeyword);

            return execute(create(GroupsSearchService.class).getGroups(searchKeyword, pageNumber, CloudConfig.FLIKER_KEY));
        } catch (Throwable e) {
            MyLogger.logCatch(GroupsCloudRepoImpl.class,"getGroups",OrangeException.map(e));

            throw OrangeException.map(e);
        }
    }
}
