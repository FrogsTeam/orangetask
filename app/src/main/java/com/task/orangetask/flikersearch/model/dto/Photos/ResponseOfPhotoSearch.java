package com.task.orangetask.flikersearch.model.dto.Photos;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class ResponseOfPhotoSearch {
    private String stat;
    private Photos photos;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
