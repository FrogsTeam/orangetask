package com.task.orangetask.flikersearch.ui.view;

import com.task.orangetask.base.view.MvpView;
import com.task.orangetask.flikersearch.model.dto.Photos.Photos;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface PhotosView extends MvpView {


    void showInlineLoading();

    void hideInlineLoading();

    void showInlineError();

    void displayPhotos(Photos photo);

    void showInlineErrorFirstTime();

    void displayPhotosFirstTime(Photos photo);

    void noDataFound();
    void noDataFoundFirstTime();

    void serverProblem();

    void serverProblemFirstTime();


}
