package com.task.orangetask.flikersearch.model.cloud.photos;

import com.task.orangetask.flikersearch.model.dto.Photos.ResponseOfPhotoSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public interface PhotosSearchService {

    @GET("?method=flickr.photos.search&format=json&nojsoncallback=1")
    Call<ResponseOfPhotoSearch> getPhotos(@Query("text") String keywordSearch, @Query("page") int pageNumber,@Query("api_key") String api_key);
}
