package com.task.orangetask;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.task.orangetask.flikersearch.ui.activity.SearchActivity;
import com.task.orangetask.base.view.BaseActivity;

public class MainActivity extends BaseActivity {

    public static final String SEARCH_TYPE_KEY = "search_type";
    public static final int SEARCH_IN_PHOTOS = 1;
    public static final int SEARCH_IN_GROUPS = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button) findViewById(R.id.search_in_flickr_photos_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goTo(SEARCH_IN_PHOTOS);
            }
        });


        ((Button) findViewById(R.id.search_in_flickr_group_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goTo(SEARCH_IN_GROUPS);

            }
        });
    }

    private void goTo(int search_type) {
        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        intent.putExtra(SEARCH_TYPE_KEY, search_type);
        startActivity(intent);

    }
}
