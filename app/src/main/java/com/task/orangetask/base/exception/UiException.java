package com.task.orangetask.base.exception;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class UiException extends Exception {

  private   String message;


    public static UiException map(OrangeException e) {

        UiException uiException=new UiException();
        uiException.setMessage(e.getMessage());
        return uiException;
    }


    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
