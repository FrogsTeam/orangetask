package com.task.orangetask.base.exception;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class OrangeException extends Exception {

    private String message;

    public static OrangeException map(Throwable t) {

        OrangeException exception = new OrangeException();

        exception.setMessage(t.getMessage());
        return exception;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
