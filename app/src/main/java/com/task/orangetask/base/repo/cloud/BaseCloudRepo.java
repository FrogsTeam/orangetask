package com.task.orangetask.base.repo.cloud;


import com.task.orangetask.base.exception.OrangeException;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Eslam Hussein on 5/14/16.
 */
public class BaseCloudRepo {

    private Map<Class<?>, Object> servicesMap;

    public BaseCloudRepo() {
        servicesMap = new HashMap<>();
    }

    protected <T> T create(Class<T> clazz) {
        T service;
        if (servicesMap.containsKey(clazz)) {
            service = (T) servicesMap.get(clazz);
        } else {
            service = retrofit().create(clazz);
            servicesMap.put(clazz, service);
        }
        return service;
    }

    protected <T> T execute(Call<T> call) throws Throwable {
        Response<T> response = call.execute();
        if (!response.isSuccess())
            throw new OrangeException();  // TODO: HTTP failure exception
        return response.body();
    }

    private Retrofit retrofit() {
        return new Retrofit.Builder()
                .baseUrl(CloudConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
